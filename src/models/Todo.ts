// Todo List Model

import * as mongoose from "mongoose";

export type TodoModel = mongoose.Document & {
    description: string,
    done: boolean
};



const TodoSchema = new mongoose.Schema({
    description: String,
    done: Boolean
}, { timestamps: true });





// export const User: UserType = mongoose.model<UserType>('User', userSchema);
const Todo = mongoose.model("Todo", TodoSchema);
export default Todo;