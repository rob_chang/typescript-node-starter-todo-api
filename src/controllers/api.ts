"use strict";

import * as async from "async";
import * as request from "request";
import * as graph from "fbgraph";
import { Response, Request, NextFunction } from "express";
import { default as Todo, TodoModel } from "../models/Todo";


//
// Todo APIs

/**
 *
 *  Todo Server API returns a JSON object in the form:
 *
 * Supports the following actions
 *
 *     getall  - Get all Todo items
 *     add - Add a Todo item
 *
 *     See routing table for URLs for this application
 *
 *  1. Getall -  Get all Todo Items
 *
 *  { todos:[
 *        {
 *          "_id" : <id>,
 *          "description" : <description>,
 *          "done" : <boolean true = done, false = not done>
 *          "createdAt" : <creation date example: 2018-02-01T18:41:18.704Z >
 *          "updatedAt" : <update date>
 *        },
 *        ...
 *      ]
 *   }
 *
 *  2. Add - Add a Todo Item
 *
 *  POST a JSON payload in the form:
 *
 *  {
 *      "description" : <description>
 *      "done" : <boolean true = done, false = not done>
 *   }
 */


export let addTodo = async (req: Request, res: Response) => {
  const description = req.body.description;

  const item = new Todo({
    description: description,
    done: false
  });
  const savedItem = await item.save();
  res.json(savedItem);
};

export let getTodos = async (req: Request, res: Response) => {

  try {
    const todoItems = await Todo.find({});

    const reply = {
      todos: todoItems
    };
    res.json(reply);
  } catch (e) {
    res.status(500).send("");
  }
};

/**
 * GET /api
 * List of API examples.
 */
export let getApi = (req: Request, res: Response) => {
  res.render("api/index", {
    title: "API Examples"
  });
};

/**
 * GET /api/facebook
 * Facebook API example.
 */
export let getFacebook = (req: Request, res: Response, next: NextFunction) => {
  const token = req.user.tokens.find((token: any) => token.kind === "facebook");
  graph.setAccessToken(token.accessToken);
  graph.get(`${req.user.facebook}?fields=id,name,email,first_name,last_name,gender,link,locale,timezone`, (err: Error, results: graph.FacebookUser) => {
    if (err) { return next(err); }
    res.render("api/facebook", {
      title: "Facebook API",
      profile: results
    });
  });
};
